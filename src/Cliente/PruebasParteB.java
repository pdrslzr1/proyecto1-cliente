package Cliente;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

import com.google.gson.stream.JsonReader;

import estructuras.ListaDoblePrinceton;
import estructuras.QueuePrinceton;
import mundo.Colision;
import mundo.WardsBo;

public class PruebasParteB {
	
	BufferedWriter escritor;
	Scanner lector;
	
	public final static String PATH = "./docs/Boroughs.json";
	
	public final static String SEPARADOR_WARDS = " ";

    private static ListaDoblePrinceton<WardsBo> lista;
	
	private static QueuePrinceton<String> colaSolicitudes;
	
	private static int ULTIMO_ANO_DATO=2014;
	//TODO: Declarar objetos de la parte B
	
	public PruebasParteB(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
		
		lista = new ListaDoblePrinceton<WardsBo>();

		colaSolicitudes = new QueuePrinceton<>();
		
	}
	
	public static void lecuraWards(){
		if (lista.isEmpty()) {
			
		
		try {
 
			FileInputStream inWard = new FileInputStream(new File(PATH));

			JsonReader reader = new JsonReader(new InputStreamReader(inWard, "UTF-8"));

			reader.beginArray();

			lecturaSolicitudes();
			while (reader.hasNext()) {
			
				
			
				WardsBo wd = new WardsBo();
				crearWards(reader,wd);
            
             
          
	
			}
			reader.close();


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
	public static void crearWards(JsonReader in,WardsBo wd) {
	
		try {

			
			String nombre = null;
		
			String[] colision = null;
			int cantidad = 0;
			
			
			in.beginObject();
			
			while (in.hasNext()) {

			
				
				String ward = in.nextName();
				if (ward.startsWith("Area")) {
					
					nombre = in.nextString();

					   wd.modificarWard(nombre);
						lista.add(wd);

				}
			

				if (ward.startsWith("Fatal")) {

					colision = ward.split(SEPARADOR_WARDS);
					cantidad = in.nextInt();

					Colision c = new Colision(colision[0], Integer.parseInt(colision[1]), cantidad);
					wd.agregarColision(c);
					

				}
				if (ward.startsWith("Serious")) {

					colision = ward.split(SEPARADOR_WARDS);
					cantidad = in.nextInt();

					Colision c = new Colision(colision[0], Integer.parseInt(colision[1]), cantidad);
					wd.agregarColision(c);
					

				}
				if (ward.startsWith("Slight")) {

					colision = ward.split(SEPARADOR_WARDS);
					cantidad = in.nextInt();

					String y = Integer.toString(cantidad);
					Colision c = new Colision(colision[0], Integer.parseInt(colision[1]), cantidad);
					wd.agregarColision(c);
					
				
					

				}
				if (ward.startsWith("Total")) {

				in.nextInt();
				

				}
				
			}
			wd = null;
			in.endObject();
		

		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	
	public static void lecturaSolicitudes(){
		
		FileReader file;
		String[] solicitud =null;
		try {
			file = new FileReader("./docs/Solicitudes_Alcaldes.csv");
			  BufferedReader lec = new BufferedReader(file );
			  
			  String x ="";
			  while ((x=lec.readLine())!=null) {
				  
				  solicitud =x.split(";");
				  if(!solicitud[0].equals("Local Authority")){
				  colaSolicitudes.enqueue(solicitud[0]);
				  
				
				  
				  }
				
			}
		        
		         
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void pruebas() {
		int opcion = -1;
		
		//TODO: Inicializar objetos de la parte B
		
		
		long tiempoDeCarga = System.nanoTime();
		//TODO: Cargar informacion de la parte B
		lecuraWards();
		
		tiempoDeCarga = System.nanoTime() - tiempoDeCarga;
		
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto B---------------\n");
				escritor.write("Informacion cargada en: " + tiempoDeCarga + " nanosegundos\n");
				escritor.write("Reportes:\n");
				escritor.write("1: Generar reporte de colisiones Serious que\n     estan por encima del maximo permitido\n");
				escritor.write("2: Area con el indice de colisiones Serios mas\n     alto en un periodo de tiempo\n");
				escritor.write("3: Historial de colisiones Serios para un area\n");
				escritor.write("4: La opcion 3 hara este reporte con la cola de solicitudes (acordado en clase)\n");
				escritor.write("5: La opcion 3 hara este reporte con la cola de solicitudes (acordado en clase)\n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: reporte1(); break;
				case 2: reporte2(); break;
				case 3: reporte3(); break;
				case 4: reporte3(); break;
				case 5: reporte3();; break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	private void reporte1() throws IOException{
		long tiempo = System.nanoTime();
		//TODO: Generar reporte de colisiones Serios que estan por encima del maximo permitido
		System.out.println("=======Serious colisions by cities=======");
		
		Iterator<WardsBo> ite = lista.iterator();
		while (ite.hasNext()) {

			WardsBo x = ite.next();

			
			Iterator<Colision> ite2 = x.darLista().iterator();

			while (ite2.hasNext()) {

				Colision y = ite2.next();

				if (y.darTipo().equals("Serious")&&y.darCantidad()>100 ) {
					System.out.println("El area de "+x.darWard()+"Presenta una cantidad de "+y.darCantidad()+" en el a�o "+y.darAno());
				}

			}
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte2() throws IOException{
		escritor.write("Ingrese un periodo de tiempo con el formato AAAA-AAAA\n");
		escritor.flush();
		lector.nextLine();
		String[] periodo = lector.nextLine().trim().split("-");
		if (periodo.length < 2) {
			throw new NumberFormatException();
		}
		int inicio = Integer.parseInt(periodo[0].trim());
		int fin = Integer.parseInt(periodo[1].trim());
		long tiempo = System.nanoTime();
		//TODO: Generar reporte del area con el indice de colisiones Serious mas alto en el rango de fechas 'inicio'-'fin'
		
		Iterator<WardsBo> ite = lista.iterator();

		int contador = 0;
		String ward = "";
		String autoridad = "";
		int contador2 = 0;
		while (ite.hasNext()) {

			WardsBo x = ite.next();

			Iterator<Colision> ite2 = x.darLista().iterator();

			while (ite2.hasNext()) {

				Colision y = ite2.next();

				if (y.darAno() <= fin && y.darAno() >= inicio && y.darTipo().equals("Serious")) {
					contador2 += y.darCantidad();

				}

			}
			if (contador2 > contador) {

				contador = contador2;

				ward = x.darWard();

				

			}
		}

		System.out.println("La zona con mas alarmas Serious es:  " + ward + " con " + contador + " choques.");

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Inicio: " + inicio + "\n");
		escritor.write("fin: " + fin + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void reporte3() throws IOException{
//		escritor.write("Ingrese un area\n");
//		escritor.flush();
//		lector.nextLine();
//		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar reporte historial de colisiones Serious para el 'area'
		
		String zona = colaSolicitudes.dequeue();
		System.out.println("La solicitud en cola es: "+zona);
		Iterator<WardsBo> ite = lista.iterator();
		while (ite.hasNext()) {

			WardsBo x = ite.next();
			

			if (x.darWard().equalsIgnoreCase(zona)) {
				Iterator<Colision> ite2 = x.darLista().iterator();

				

				while (ite2.hasNext()) {

					Colision y = ite2.next();

					if (y.darTipo().equals("Serious")) {

						System.out.println(
								"====REPORTE 3============ Area: " + x.darWard() + " Colisiones: " + y.darCantidad() + " Fecha: " + y.darAno());

						
					}

				}
			}
		}
		reporte4(zona);
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + zona + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte4(String zona) throws IOException{
//		escritor.write("Ingrese un area\n");
//		escritor.flush();
//		lector.nextLine();
//		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar el promedio de colisiones Serious para el 'area'
		
		int contador =0;
		int cantidad =0;
		String nombre ="";
		Iterator<WardsBo> ite = lista.iterator();

		while (ite.hasNext()) {

			WardsBo x = ite.next();

			if (x.darWard().equalsIgnoreCase(zona)) {
				Iterator<Colision> ite2 = x.darLista().iterator();
nombre=x.darWard();
				

				while (ite2.hasNext()) {

					Colision y = ite2.next();

					if (y.darTipo().equals("Serious")) {

						contador += y.darCantidad();
						cantidad += 1;

						
					}

				}
			}
		}
		System.out.println("=====REPORTE 4======El promedio para el area: " + nombre + " es de: " + contador / cantidad
				+ " entre el 2010 y 2014");
		
		reporte5(zona);

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + zona + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte5(String zona) throws IOException{
//		escritor.write("Ingrese un area\n");
//		escritor.flush();
//		lector.nextLine();
//		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar el valor mas reciente (ultimo ano) de colisiones Serious para el 'area'
		
Iterator<WardsBo> ite = lista.iterator();
		
		while (ite.hasNext()) {

			WardsBo x = ite.next();

			if (x.darWard().equalsIgnoreCase(zona)) {
				Iterator<Colision> ite2 = x.darLista().iterator();

				

				while (ite2.hasNext()) {

					Colision y = ite2.next();

					if (y.darTipo().equals("Serious")&&y.darAno()>=ULTIMO_ANO_DATO) {

				
						
						System.out.println(
								"========REPORTE 5======== El ultimo reporte de colisiones serious para "+zona+" es de: "+y.darCantidad()+"En  el a�o "+y.darAno());

					}

				}
			}
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + zona + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
}