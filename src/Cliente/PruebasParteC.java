package Cliente;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import mundo.Central;


public class PruebasParteC {
	
	BufferedWriter escritor;
	Scanner lector;
	Central central;
	
	public PruebasParteC(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;

		
	}
	
	
	
	public int lecturaIngresos() {
		//no está claro si debo ordenarlos al entrar...

		int pasos = 0;
		FileReader file;
		String[] solicitud = null;
		try {
			file = new FileReader("./docs/Ingreso_al_parqueadero.csv");
			BufferedReader lec = new BufferedReader(file);
			String x;

			while ((x = lec.readLine()) != null) {

				solicitud = x.split(";");

				central.park(solicitud[3], solicitud[2] , solicitud[1] + " " + solicitud[0]);

				pasos++;

			}
			System.out.println("MANDA EXCEPTION PERO FUNCIONA BIEN NO ENCONTR� EL POR QU�");
			return pasos;

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
		return pasos;

	}

public int lecturaSalida(){
		int pasos =0;
		FileReader file;
		String[] solicitud =null;
		try {
			file = new FileReader("./docs/Salida_del_parqueadero.csv");
			  BufferedReader lec = new BufferedReader(file );
			  String x=lec.readLine();
			  
			  while ((x=lec.readLine())!=null) {
				  solicitud = x.split(";");
				  
				 pasos=central.remove("AULC18");
				 
			}
		         
		         
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pasos;
		
	}
	
	public void pruebas() {
		try {
			//TOTO: Inicializar objetos de la parte C
			central = new Central();
			
			
			
			int pasosIngreso = 0;
			//TODO: Calcular el numero de pasos necesario para parquear todos los carros
			//		y guardar el numero en la variable 'pasosIngreso'
			
			
			escritor.write("Organizacion del parqueadero luego del ingreso\n     de los alcaldes:\n");
			escritor.flush();
			//TODO: Generar impresion de parqueadero luego del ingreso de todos los alcaldes
			//		(ver ejemplo abajo)
			
			
		    int pasosSalida =0;
			//TODO: Calcular el numero de pasos necesario para sacar todos los carros
			//		y guardar el numero en la variable 'pasosSalida'
			
			
			escritor.write("Numero de pasos ingreso: " + pasosIngreso + "\n");
			escritor.write("NUmero de pasos salida: " + pasosSalida + "\n");
			escritor.write("Ingrese cualquier letra y Enter para continuar\n");
			escritor.flush();
			lector.nextLine();
			lector.nextLine();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}

/*
Ejemplo impresion del parqueadero luego del ingreso de todos los alcaldes
En cada puesto aparece el numero de entrada del alcalde junto con sus iniciales
____________________________________________
____________________________________________
| |      ||      ||      ||      ||      | |
| | 1DH  || 5JP  || 9SK  || 13NF || 17DT | |
| |      ||      ||      ||      ||      | |
| | ---- || ---- || ---- || ---- || ---- | |
| |      ||      ||      ||      ||      | |
| | 2MR  || 6PS  || 10KA || 14PD ||      | |
| |      ||      ||      ||      ||      | |
| | ---- || ---- || ---- || ---- || ---- | |
| |      ||      ||      ||      ||      | |
| | 3MS  || 7SB  || 11DB || 15GO ||      | |
| |      ||      ||      ||      ||      | |
| | ---- ||----- || ---- || ---- || ---- | |
| |      ||      ||      ||      ||      | |
| | 4RJ  || 8JA  || 12RW || 16JB ||      | |
| |      ||      ||      ||      ||      | |
| | ---- || ---- || ---- || ---- || ---- | |
| |      ||      ||      ||      ||      | |
*/