package Cliente;

import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

import com.google.gson.stream.JsonReader;

import estructuras.ListaDoblePrinceton;
import estructuras.QueuePrinceton;
import mundo.Colision;
import mundo.Ward;

public class PruebasParteA {
	
	public final static String PATH = "./docs/Ward.json";

	public final static String SEPARADOR_WARDS = " ";

    private static ListaDoblePrinceton<Ward> lista;
	
	private static QueuePrinceton<String> colaSolicitudes;
	
	BufferedWriter escritor;
	Scanner lector;
	
	//TODO: Declarar objetos de la parte A
	
	public PruebasParteA(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
		
		lista = new ListaDoblePrinceton<Ward>();

		colaSolicitudes = new QueuePrinceton<>();
	}
	
public static void lecuraWards(){
		if (lista.isEmpty()) {
			
		
		try {
 
			FileInputStream inWard = new FileInputStream(new File(PATH));

			JsonReader reader = new JsonReader(new InputStreamReader(inWard, "UTF-8"));

			reader.beginArray();

			lecturaSolicitudes();
			while (reader.hasNext()) {
			
				
			
				Ward wd = new Ward();
				crearWards(reader,wd);
            
             
      
			}
			reader.close();


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
	public static void crearWards(JsonReader in,Ward wd) {
	
		try {

			
			String nombre = null;
			String autoridad = null;
			String[] colision = null;
			int cantidad = 0;
			
			
			in.beginObject();
			
			while (in.hasNext()) {

			
				
				String ward = in.nextName();
				if (ward.startsWith("Ward name")) {
					
					nombre = in.nextString();

				}
				if (ward.startsWith("Local Authority")) {

					autoridad = in.nextString();

				   wd.modificarWard(nombre, autoridad);
					lista.add(wd);
				}

				if (ward.startsWith("Fatal")) {

					colision = ward.split(SEPARADOR_WARDS);
					cantidad = in.nextInt();

					Colision c = new Colision(colision[0], Integer.parseInt(colision[1]), cantidad);
					wd.agregarColision(c);
					

				}
				if (ward.startsWith("Serious")) {

					colision = ward.split(SEPARADOR_WARDS);
					cantidad = in.nextInt();

					Colision c = new Colision(colision[0], Integer.parseInt(colision[1]), cantidad);
					wd.agregarColision(c);
				

				}
				if (ward.startsWith("Slight")) {

					colision = ward.split(SEPARADOR_WARDS);
					cantidad = in.nextInt();

					String y = Integer.toString(cantidad);
					Colision c = new Colision(colision[0], Integer.parseInt(colision[1]), cantidad);
					wd.agregarColision(c);
					
				
					

				}
				if (ward.startsWith("Total")) {

				in.nextInt();
				

				}
				
			}
			wd = null;
			in.endObject();
		

		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	
	public static void lecturaSolicitudes(){
		
		FileReader file;
		String[] solicitud =null;
		try {
			file = new FileReader("./docs/Solicitudes_Alcaldes.csv");
			  BufferedReader lec = new BufferedReader(file );
			  String x = "";
			  
			  while ((x=lec.readLine())!=null) {
				  
				
				  solicitud = x.split(";");
				  if(!solicitud[0].equals("Local Authority")){
				  colaSolicitudes.enqueue(solicitud[0]);

				  }
			}
		         
		         
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
	public void pruebas() {
		int opcion = -1;
		
		//TODO: Inicializar objetos de la parte A
	
		lecuraWards();
		lecturaSolicitudes();
		
		long tiempoDeCarga = System.nanoTime();
		//TODO: Cargar informacion de la parte A
		
		
		tiempoDeCarga = System.nanoTime() - tiempoDeCarga;
		
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto A---------------\n");
				escritor.write("Informacion cargada en: " + tiempoDeCarga + " nanosegundos\n");
				escritor.write("Reportes:\n");
				escritor.write("1: Generar reporte de autoridades locales con mas\n     de 40 colisiones Slight al ano\n");
				escritor.write("2: Barrios en que mas generaron alarmas Slight\n     en un periodo de tiempo\n");
				escritor.write("3: Historial de colisiones Slight para un barrio\n");
				escritor.write("4: La opci�n 3 generara este reporte con el barrio dado y la solicitud en cola, si estas existen(acordado en clase)\n");
				escritor.write("5: La opci�n 3 generara este reporte con el barrio dado y la solicitud en cola, si estas existen(acordado en clase)\n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: reporte1(); break;
				case 2: reporte2(); break;
				case 3: reporte3(); break;
				case 4: reporte3();
				case 5: reporte3();
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	private void reporte1() throws IOException {
		long tiempo = System.nanoTime();
		// TODO: Generar reporte de las autoridades locales con mas de 40
		// colisiones Slight al ano

		Iterator<Ward> ite = lista.iterator();
		String autoridad = colaSolicitudes.dequeue();

		while (ite.hasNext()) {

			Ward x = ite.next();

			Iterator<Colision> ite2 = x.darLista().iterator();

			while (ite2.hasNext()) {

				Colision y = ite2.next();

				if (y.darTipo().equals("Slight") && y.darCantidad() > 40 ) {
					System.out.println("La autoridad " + x.darAutoridad() + " reporta una cantidad de "
							+ y.darCantidad() + " colisiones en la zona " + x.darWard() + " en el a�o " + y.darAno());

				}

			}
		}

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void reporte2() throws IOException {
		escritor.write("Ingrese un periodo de tiempo con el formato AAAA-AAAA\n");
		escritor.flush();
		lector.nextLine();
		String[] periodo = lector.nextLine().trim().split("-");
		if (periodo.length < 2) {
			throw new NumberFormatException();
		}
		int inicio = Integer.parseInt(periodo[0].trim());
		int fin = Integer.parseInt(periodo[1].trim());
		long tiempo = System.nanoTime();
		// TODO: Generar reporte del barrio que genero mas alarmas Slight en el
		// rango de fechas 'inicio'-'fin'

		Iterator<Ward> ite = lista.iterator();
		int contador = 0;
		String ward = "";
		String autoridad = "";
		int contador2 = 0;
		while (ite.hasNext()) {

			Ward x = ite.next();

			Iterator<Colision> ite2 = x.darLista().iterator();

			

			while (ite2.hasNext()) {

				Colision y = ite2.next();

				if (y.darAno() <= fin && y.darAno() >= inicio&&y.darTipo().equals("Slight")) {

					
					contador2 += y.darCantidad();



				}
				
				
			}
			if (contador2 > contador) {

				contador = contador2;
				autoridad = x.darAutoridad();
				ward = x.darWard();

				System.out.println(contador);
				

			}
		
			contador2 =0;
			System.out.println(contador2);
		}

		System.out.println("La zona con mas alarmas Slight es:  " + ward + " con autoridad " + autoridad + " y " + contador
				+ " choques.");

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Inicio: " + inicio + "\n");
		escritor.write("fin: " + fin + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte3() throws IOException {
		escritor.write("Ingrese un barrio\n");
		escritor.flush();
		lector.nextLine();
		String barrio = lector.nextLine();
		long tiempo = System.nanoTime();
		
		boolean encontrado = false;
		// TODO: Generar reporte historial de colisiones Slight para el 'barrio'

		Iterator<Ward> ite = lista.iterator();
		
		String autoridad = colaSolicitudes.dequeue();
		while (ite.hasNext()) {

			Ward x = ite.next();

			if (x.darWard().equalsIgnoreCase(barrio)&&autoridad.equals(x.darAutoridad())) {
				Iterator<Colision> ite2 = x.darLista().iterator();

				encontrado =true;

				while (ite2.hasNext()) {

					Colision y = ite2.next();

					if (y.darTipo().equals("Slight")) {

						System.out.println(
								"=====REPORTE 3=====  \n Barrio: " + x.darWard() + " Colisiones: " + y.darCantidad() + " Fecha: " + y.darAno());
                               reporte4(autoridad, barrio);
						
					}

				}
			}
		}
		if(encontrado==false){
			System.out.println("No se a econtrado el ward "+barrio+" con autoridad "+ autoridad+" Se ha sacado la solicitud  de la cola para atender las otras solicitudes ya que esta convinacion no permite realizar ninguno de los ultimos 3 reportes (en clase se decidio en unir los ultimos 3 reportes)");
		
		
		}
		
			
		

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + barrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void reporte4(String pAutoridad,String pBarrio) throws IOException {
//		escritor.write("Ingrese un barrio\n");
//		escritor.flush();
//		lector.nextLine();
//		String barrio = lector.nextLine();
		
		
		long tiempo = System.nanoTime();
		int contador = 0;
		int cantidad = 0;
		String nombre ="";
		// TODO: Generar el promedio de colisiones Slight para el 'barrio'

		Iterator<Ward> ite = lista.iterator();

		while (ite.hasNext()) {

			Ward x = ite.next();

			if (x.darWard().equalsIgnoreCase(pBarrio) && x.darAutoridad().equalsIgnoreCase(pAutoridad)) {
				Iterator<Colision> ite2 = x.darLista().iterator();

				System.out.println(x.darWard() + "AUTORIDAD: " + x.darAutoridad());
				nombre = x.darWard();
				while (ite2.hasNext()) {

					Colision y = ite2.next();

					if (y.darTipo().equals("Slight")) {

						contador += y.darCantidad();
						cantidad += 1;

						

					}

				}
			}
		}
		if(nombre!=""){
		System.out.println("=======Reporte 4====== \n El promedio para la zona: " + nombre + " es de: " + contador / cantidad
				+ " entre el 2010 y 2014");
		
		reporte5(pAutoridad,pBarrio);
		
		}
		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + pBarrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void reporte5(String pAutoridad, String pBarrio) throws IOException{
//		escritor.write("Ingrese un barrio\n");
//		escritor.flush();
//		lector.nextLine();
//		String barrio = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar el valor mas reciente (ultimo ano) de colisiones Slight para el 'barrio'
		
Iterator<Ward> ite = lista.iterator();
		
		while (ite.hasNext()) {

			Ward x = ite.next();

			if (x.darWard().equalsIgnoreCase(pBarrio)) {
				Iterator<Colision> ite2 = x.darLista().iterator();

				System.out.println(x.darWard() + "AUTORIDAD: " + x.darAutoridad());

				while (ite2.hasNext()) {

					Colision y = ite2.next();

					if (y.darTipo().equals("Slight")&&y.darAno()>=2014) {

				
						
						System.out.println(
								"=====REPORTE 5========= \n El ultimo reporte de colisiones slight es de: "+y.darCantidad()+" En  el a�o "+y.darAno()+"\n se a sacado la solicitud de la cola para atender las otras solicitudes");

					}

				}
			}
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + pBarrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
}