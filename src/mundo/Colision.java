package mundo;

public class Colision {
	
	private int ano;
	
	private String tipo;
	
	private int cantidad;
	
	public Colision(String pTipo,int pAno ,int pCantidad){
		
		ano = pAno;
		
		tipo = pTipo;
		
		cantidad = pCantidad;
				
	}
	
	public int darAno(){
		return ano;
	}

	public String darTipo(){
		return tipo;
	}
	
	public int darCantidad(){
		
		return cantidad;
	}

}
