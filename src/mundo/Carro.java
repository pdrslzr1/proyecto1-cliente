package mundo;


import java.util.Date;

@SuppressWarnings("ALL")
public class Carro
{
   /**
    * Color del vehiculo
    */
   private String color;
   /**
    * Matricula del vehiculo
    */
   private String matricula;
   /**
    * nombre del due�o del vehiculo
    */
   private String nombreConductor;
   /**
    * Registro ingreso del vehiculos
    */
   private int puesto;
 
   public Carro (String pColor, String pMatricula, String pNombreConductor, int puesto)
  {
		color = pColor;
		matricula = pMatricula;
		nombreConductor = pNombreConductor;
        this.puesto = puesto;
	 
   }
   
   /**
    * Da el color del vehiculo
    * @return el color del vehiculo
    */
   public String darColor()
   {
	   return color;
   }
   /**
    * Da la matricula del vehiculo
    * @return la matricula del vehiculo
    */
   public String darMatricula()
   {
	   return matricula;
   }
   /**
    * Da el nombre de quien conduce el vehiculo
    * @return el nombre de quien conduce el vehiculo
    */
   public String darNombreConductor ()
   {
	   return nombreConductor;
   }
	
}
