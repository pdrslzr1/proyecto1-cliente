package mundo;

import estructuras.LinkedStack;
import estructuras.QueuePrinceton;

import java.util.Collections;

@SuppressWarnings("ALL")

public class Central {

    LinkedStack<Carro>[] slots;
	
    LinkedStack<Carro> temp;

    QueuePrinceton<Carro> wait;

    Carro[] entrada;

    Carro[] salida;


	
	public Central(){
		
		slots = new LinkedStack[4];

		for(int i=0; i<slots.length;i++){
			
			slots[i]=new LinkedStack<Carro>();
	
		}
        wait = new QueuePrinceton<Carro>();
		temp = new LinkedStack<Carro>();
	}

	
	 public void park(String pColor,String pMatricula, String pNombre) {

         Carro car = new Carro(pColor, pMatricula, pNombre,3);

         int slot = available();
         if(slot != -1){
             slots[slot].push(car);
         }
         else{
             wait.enqueue(car);
         }
     }
	 
	 public int available() {

        int ans = -1;//reponde -1 si no hay parqueaderos disponibles, de lo contrario responde en qué stack hay cupo
        for (int i = 0; i < slots.length; i++) {

            if(slots[i].size()<5){
                ans =  i;
                break;
            }
        }
        return ans;
    }
	 
    public int remove(String pmatricula){//esto optimiza la sacada de vehiculos asumiendo desconocimiento del orden de salida al llegar (como en la vida real) no tendria sentido saber eso...
    int a = 0;
        for (LinkedStack<Carro> l: slots) {
            for (Carro c : l) {
                if(c.darMatricula().equals(pmatricula)){
                    if(l.peek().equals(c)){
                        l.pop();
                        a++;
                    }
                    else{
                        while(!l.peek().equals(c)){
                            temp.push(l.pop());
                            a++;
                        }
                        l.pop();
                        while(!temp.isEmpty()){
                            l.push(temp.pop());
                            a++;
                        }
                    }
                    break;
                }
            }
        }
        if(!wait.isEmpty()){
            slots[available()].push(wait.dequeue());
        }
        return a;
    }


}
